import test from "ava";
import { LibEvent } from './libevent'

test("Should generate only one instance of LibEvent", expect => {
  let previousInstance = LibEvent.singleton()

  for (const _ of new Array(10).fill(null)) {
    const instance = LibEvent.singleton()

    expect.is(instance, previousInstance)

    previousInstance = instance
  }
})

test("Should register three events", (expect) => {
  const event = new LibEvent()
  const log = () => undefined

  event
    .on('foo', log)
    .on('bar', log)
    .on('make.noise', log)

  expect.is(event.events.length, 3);
})

test("Should fire the same event three times", (expect) => {
  let times = 0
  const event = new LibEvent()
  const actions = [
    () => times++,
    () => times++,
    () => times++,
  ]

  actions.forEach((action) => event.on('action', action))
  event.emit("action", { data: null })

  expect.is(times, 3)
})

test('Should fire an event and receive a massage', (expect) => {
  const event = new LibEvent()
  const message = 'the event was emtestted'
  const eventName = "test"

  event.on<string>(eventName, ({ data }) => expect.is(data, message))
  event.emit(eventName, { data: message })
})

test('Should fire an event and receive a array of strings', (expect) => {
  const args = [
    'arg1',
    'arg2',
    'kwarg',
    'details'
  ]

  const event = new LibEvent()

  event.on<string[]>('args', ({ data }) => expect.deepEqual(data, args))
  event.emit('args', { data: args })
})

test('Should unregister a event', (expect) => {
  const event = new LibEvent()
  let count = 0
  const increment = () => ++count

  event
    .on('actions', increment)
    .emit('actions', { data: null })
    .off('actions', increment)
    .emit('actions', { data: null })

  expect.is(count, 1)
})

test('Deve remover uma ação especifica do evento', (expect) => {
  let counter = 0
  const event = new LibEvent()
  const fn1 = () => counter++

  event
    .on('actions', fn1)
    .emit("actions", { data: null })
    .on('actions', fn1)
    .off('actions', fn1)
    .emit("actions", { data: null })


  expect.is(counter, 1);
})

test("Should remove an event register after remove all actions", expect => {
  const event = new LibEvent()
  const action = "foo"
  const fn = () => undefined;

  event.on(action, fn)

  expect.is(event.events.length, 1)

  event.off(action, fn);

  expect.is(event.events.length, 0)
})

test('Should remove all functions from an event', (expect) => {
  let counter = 0

  const event = new LibEvent()
  const fn = () => counter++

  event
    .on('action', fn)
    .emit("action", { data: null })
    .on('action', fn)
    .destroy('action')
    .emit("action", { data: null })

  expect.is(counter, 1)
  expect.is(event.events.length, 0)
});
