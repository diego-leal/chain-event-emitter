interface LibEventAction {
  fn: (data: LibEventData<any>) => void,
}

interface LibEventRegister {
  [key: string]: LibEventAction[] | undefined
}

interface LibEventData<T> {
  data: T
}

let instance: LibEvent | null = null

export class LibEvent {
  #register: LibEventRegister = {}

  constructor () {}

  private _subscribe(eventKey: string, data: LibEventAction) {
    this.#register[eventKey] = this.#register[eventKey] || []
    this.#register[eventKey]?.push(data)
  }

  get events (): string[] {
    return Object.keys(this.#register)
  }

  on<T>(eventKey: string, fn: (data: LibEventData<T>) => void): LibEvent {
    this._subscribe(eventKey, { fn })
    return this
  }

  emit<T>(eventKey: string, data: LibEventData<T>): LibEvent {
    const events = this.#register[eventKey] ?? []
    events.forEach((event) => event.fn(data))
    return this
  }

  off (eventKey: string, fn: Function): LibEvent {
    this.#register[eventKey] = this.#register[eventKey]?.filter((event) => event.fn !== fn)

    // this is to avoid memory leak
    if (!this.#register[eventKey]?.length) {
      Reflect.deleteProperty(this.#register, eventKey)
    }

    return this
  }

  destroy (eventKey: string): LibEvent {
    Reflect.deleteProperty(this.#register, eventKey)
    return this
  }

  static singleton () {
    if (!instance) {
      instance = new LibEvent()
      return instance
    }

    return instance;
  }
}
